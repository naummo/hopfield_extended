set -x
conda create --name abfs python=3.4
source activate abfs
pip install --upgrade pip
pip install jupyter jupyter_contrib_nbextensions matplotlib scipy moviepy requests
