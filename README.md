# Extended Hopfield Recurrent Neural Network

**TL;DR** -- a video simulation of this RNN inference is available [HERE](https://www.youtube.com/watch?v=nZDe2NsCSmI)

This is reproduction of the work done by Nagao et al in [1] in simulating multistable perception in humans using an extended version of the Hopfield Recurrent Neural Network (RNN). By representing a wide range of biological phenomena in activation function, Nagao et al created a biologically plausible model of multistable fixation in visual perception. 

All source code is stored in *HopfieldExtended.ipynb* Jupyter Notebook.

## Multistable perception
The process of multistable perception is most easily explained on the example of Necker's cube:
![alt text](necker_cube.png "Necker's cube")

Here we fixate on one of two possible interpretations of the stimulus.

## Stimulus

In this project, Necker's cube is replaced by a matrix of square cells:

![alt text](original_kompmodel.png "Original matrix")

Two "interpretations" are produced from the original stimulus by flipping the colours of the randomly chosen non-overlapping 15 squares.

![alt text](kompmodel.png "The two interpretations")

## Simulation (with video)

The neural network is trained on these two interpretations; during simulation, RNN receives the original stimulus as input. Since both interpretations are equidifferent derivations of the input image, RNN fluctuates between both. The video of the simulation containing RNN outputs for a series of timesteps is accessible on YouTube: [VIDEO LINK](https://www.youtube.com/watch?v=nZDe2NsCSmI)

## Analysis

This project also includes several model analysis tools, such as neural network energy graph and Spearman correlation analysis. The histogram below shows that fixation duration frequency follows Gamma distribution:

![alt text](frequency_distribution.png "Frequency distribution")

RNN energy analysis also highlights fluctation between two interpretations.

![alt text](energy_overlap_600_iterations.png "Energy plot")






# Bibliography
[1] Nagao, Natsuki, Haruhiko Nishimura, and Nobuyuki Matsui. "A neural chaos model of multistable perception." Neural Processing Letters 12.3 (2000): 267-276.